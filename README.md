# C11 Concurrent Demos (Engilsh | [中文](https://github.com/regding/C11_Concurrent_Demos/blob/master/README_zh.md))

The demos of C11 atomic operations library and thread support library.

Because the C standard library implementations in various environments do not necessarily support the features of C11, this demo uses [Alpine Linux](https://alpinelinux.org/) based on the [musl](https://www.musl-libc.org/) library as a compile and run environment. For convenience, use docker to compile the runtime environment.

# Build runtime environment
## Download source code
```bash
git clone git@github.com:regding/C11_Concurrent_Demos.git
cd C11_Concurrent_Demos
```

## Build docker image or pull [frolvlad/alpine-gcc](https://hub.docker.com/r/frolvlad/alpine-gcc/)
Build
```bash
docker build -t alpine-gcc:v1 .
```

Pull [frolvlad/alpine-gcc](https://hub.docker.com/r/frolvlad/alpine-gcc/)
```bash
docker pull frolvlad/alpine-gcc
```

# Compiler and run demos
e.g. Compiler and run test.c code.
```
.
├── Dockerfile
├── README.md
├── README_zh.md
├── atomic
│   └── test.c
└── thread
```

## Compiler
```bash
docker run --rm -v `pwd`:/tmp frolvlad/alpine-gcc gcc --static /tmp/atomic/test.c -o /tmp/atomic/test
```
or
```bash
docker run --rm -v `pwd`:/tmp alpine-gcc:v1 gcc --static /tmp/atomic/test.c -o /tmp/atomic/test
```

## Run
```bash
docker run --rm -v `pwd`:/tmp frolvlad/alpine-gcc /tmp/atomic/test
```
or
```bash
docker run --rm -v `pwd`:/tmp alpine-gcc:v1 /tmp/atomic/test
```

# Reference
[cppreference](http://en.cppreference.com/w/)
