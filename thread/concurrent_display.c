/**
 * Dems of the function used:
 *
 * int thrd_create( thrd_t *thr, thrd_start_t func, void *arg );
 *
 * int thrd_join( thrd_t thr, int *res );
 *
 * int thrd_sleep( const struct timespec* time_point,
 *                 struct timespec* remaining );
 **/

#include <threads.h>
#include <stdio.h>

#define NUM_THREADS 10

// thread task
int display_task(void* arg);

int main(void) {
    // object type identifying a thread
    thrd_t threads[NUM_THREADS];
    // argument to pass to the display_task function
    int thread_arg[NUM_THREADS];

    // init arguments
    for (int i = 0; i < NUM_THREADS; i++) {
        thread_arg[i] = i;
    }

    // create threads & running it
    for (int i = 0; i < NUM_THREADS; i++) {
        if (thrd_create((threads + i),  // pointer to memory location to put the identifier of the new thread
                    display_task,   // function to execute
                    (thread_arg + i))   // argument to pass to the function
                != thrd_success) {  // thrd_success if successful, thrd_error otherwise.
            printf("thread create error!\n");
        }
    }

    // main thread sleep 1 sec
    thrd_sleep(&(struct timespec){.tv_sec = 1}, NULL);

    // blocks main thread until all other threads terminates
    for (int i = 0; i < NUM_THREADS; i++) {
        thrd_join(threads[i], NULL);
    }

    return 0;
}

int display_task(void* arg) {
    printf("thread %d is running!\n", *(int*)arg);
    return 0;
}
