#ifndef _STACK_H_
#define _STACK_H_

#include <stdbool.h>

// data node of stack struct
typedef struct _Stack_node {
    struct _Stack_node* next;
    void* data;
} stack_node;

/** 
 * stack struct
 *              +----+    +----+    +----+
 *   HEAD IN -> |    | -> |    | -> |    |
 *              +----+    +----+    +----+
 **/
typedef struct _Stack {
    stack_node* head;
    int size;
} stack;

// create new stack
stack* create_stack(void);

// push a item into stack
void push(stack* s, void* data);

// pop a item from stack
void* pop(stack* s);

// check stack is empty or not
bool is_empty(stack* s);

// return size of stack
int size(stack* s);

// display stack by string, just for test
void display_stack(stack* s);

#endif//_STACK_H_
