/**
 * Dems of the function used:
 *
 * int mtx_init( mtx_t* mutex, int type );
 *
 * int mtx_lock( mtx_t* mutex );
 *
 * int mtx_unlock( mtx_t *mutex );
 *
 * void mtx_destroy( mtx_t *mutex );
 **/

#include <threads.h>
#include <stdio.h>

#define NUM_THREADS 10

// count task
static int counter(void* arg);

// count value
int count = 0;

// the lock
mtx_t mutex;

int main(void) {
    // create a simple, non-recursive mutex
    mtx_init(&mutex, mtx_plain);

    // create and running threads
    thrd_t threads[NUM_THREADS];
    for (int i = 0; i < NUM_THREADS; i++) {
        thrd_create(&threads[i], counter, NULL);
    }

    // blocks main thread until all other threads terminates
    for (int i = 0; i < NUM_THREADS; i++) {
        thrd_join(threads[i], NULL);
    }

    // destroy the mutex
    mtx_destroy(&mutex);

    return 0;
}

static int counter(void* arg) {
    mtx_lock(&mutex);   // lock
    count++;
    printf("Count value : %d\n", count);
    mtx_unlock(&mutex); // unlock

    return 0;
}
