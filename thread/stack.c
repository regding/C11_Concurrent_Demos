#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

stack* create_stack(void) {
    stack* s = (stack*) malloc(sizeof(stack));
    s->head = NULL;
    s->size = 0;

    return s;
}

void push(stack* s, void* data) {
    stack_node* node = (stack_node*) malloc(sizeof(stack_node));
    node->data = data;

    // add into empty stack
    if (is_empty(s)) {
        s->head = node;
        node->next = NULL;
    } else {
        node->next = s->head;
        s->head = node;
    }
    s->size++;
}

void* pop(stack* s) {
    if (is_empty(s))
        return NULL;

    stack_node* node = s->head;
    s->head = (s->head)->next;
    s->size--;

    return node->data;
}

bool is_empty(stack* s) {
    return (s->size == 0);
}

int size(stack* s) {
    return s->size;
}

void display_stack(stack* s) {
    if (is_empty(s))
        return;

    stack_node* node = s->head;
    for (int i = 0; i < s->size; i++) {
        printf(" -> %s ", (char*) node->data);
        node = node->next;
    }
    printf("\n");
}

