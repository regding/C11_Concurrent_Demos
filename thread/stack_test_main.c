#include <stdlib.h>
#include "stack.h"

int main(void) {
    stack* s = create_stack();
    push(s, "First");
    push(s, "Second");
    push(s, "Last");
    display_stack(s);

    pop(s);
    pop(s);
    display_stack(s);

    free(s);

    return 0;
}
