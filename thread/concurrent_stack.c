/**
 * Not test yet!
 *
 * Dems of the function used:
 *
 * int cnd_init( cnd_t* cond );
 *
 * int cnd_wait( cnd_t* cond, mtx_t* mutex );
 *
 * int cnd_signal( cnd_t *cond );
 *
 * void cnd_destroy( cnd_t* cond );
 **/

#include <stdio.h>
#include <threads.h>
#include "stack.h"

#define NUM_THREADS 10
#define NUM_STACK 5

// stack
stack* s;

// lock
mtx_t mutex;

// if stack is full, blocks all push task threads until stack is not full
cnd_t con_is_full;

// if static is empty, blocks all pop task threads until static is not empty
cnd_t con_is_empty;

// push task
int push_task(void* arg);

// pop task
int pop_task(void* arg);

// push a item into stack
static void concurrent_push(stack* s, void* data);

// pop a item from stack
static void* concurrent_pop(stack* s);

// check stack is full or not
static bool is_full(stack* s);

// for test
static void display_concurrent_stack(stack* s);

int main(void) {
    s = create_stack();

    // push task threads
    thrd_t push_threads[NUM_THREADS];
    // pop task threads
    thrd_t pop_threads[NUM_THREADS];
    // push task args
    int thread_data[NUM_THREADS];

    // init condition variable identifier
    cnd_init(&con_is_full);
    cnd_init(&con_is_empty);

    // create a simple, non-recursive mutex
    mtx_init(&mutex, mtx_plain);

    // init thread args
    for (int i = 0; i < NUM_THREADS; i++) {
        thread_data[i] = i;
    }

    // create threads & running it
    for (int i = 0; i < NUM_THREADS; i++) {
        thrd_create(&push_threads[i], push_task, &thread_data[i]);
        thrd_create(&pop_threads[i], pop_task, NULL);
    }

    // blocks main thread until all other threads terminates
    for (int i = 0; i < NUM_THREADS; i++) {
        thrd_join(push_threads[i], NULL);
        thrd_join(pop_threads[i], NULL);
    }

    // init condition variable identifier
    cnd_destroy(&con_is_full);
    cnd_destroy(&con_is_empty);

    display_concurrent_stack(s);

    return 0;
}

int push_task(void* arg) {
    concurrent_push(s, arg);
    return 0;
}

int pop_task(void* arg) {
    concurrent_pop(s);
    return 0;
}

static void concurrent_push(stack* s, void* data) {
    mtx_lock(&mutex);   // lock

    // if stack is full, blocks current thread until stack pop item
    if (is_full(s)) {
        cnd_wait(&con_is_full, &mutex);
    } else {
        push(s, data);
        // Unblocks one thread that currently waits on stack is empty condition
        cnd_signal(&con_is_empty);
    }
    mtx_unlock(&mutex); // unlock
}

static void* concurrent_pop(stack* s) {
    void* data;
    mtx_lock(&mutex);   // lock

    // if stack is empty, blocks current thread until any item push into stack
    if (is_empty(s)) {
        cnd_wait(&con_is_empty, &mutex);
    } else {
        data = pop(s);
        // Unblocks one thread that currently waits on stack is full condition
        cnd_signal(&con_is_full);
    }

    mtx_unlock(&mutex); // unlock

    return data;
}

static bool is_full(stack* s) {
    return (s->size == NUM_STACK);
}

static void display_concurrent_stack(stack* s) {
    if (is_empty(s))
        return;

    stack_node* node = s->head;
    for (int i = 0; i < s->size; i++) {
        printf(" -> %d ", *(int*) node->data);
        node = node->next;
    }
    printf("\n");
}

