# C11并发演示 ([Engilsh](https://github.com/regding/C11_Concurrent_Demos/blob/master/README.md) | 中文)

C11标准中原子操作库以及线程支持库的使用代码演示。

由于各个环境上的C标准库实现不一定支持C11的特性，所以此演示使用基于[musl](https://www.musl-libc.org/)库的[Alpine Linux](https://alpinelinux.org/)来作为编译及运行环境。为了方便起见，使用docker构建编译运行环境。

# 构建运行环境
## 下载演示代码
```bash
git clone git@github.com:regding/C11_Concurrent_Demos.git
cd C11_Concurrent_Demos
```

## 构建docker镜像或者拉取[frolvlad/alpine-gcc](https://hub.docker.com/r/frolvlad/alpine-gcc/)镜像
本地构建镜像
```bash
docker build -t alpine-gcc:v1 .
```

拉取[frolvlad/alpine-gcc](https://hub.docker.com/r/frolvlad/alpine-gcc/)镜像
```bash
docker pull frolvlad/alpine-gcc
```

# 编译运行演示代码
例：编译运行 test.c 代码
```
.
├── Dockerfile
├── README.md
├── README_zh.md
├── atomic
│   └── test.c
└── thread
```

## 编译
```bash
docker run --rm -v `pwd`:/tmp frolvlad/alpine-gcc gcc --static /tmp/atomic/test.c -o /tmp/atomic/test
```
或者
```bash
docker run --rm -v `pwd`:/tmp alpine-gcc:v1 gcc --static /tmp/atomic/test.c -o /tmp/atomic/test
```

## 运行
```bash
docker run --rm -v `pwd`:/tmp frolvlad/alpine-gcc /tmp/atomic/test
```
或者
```bash
docker run --rm -v `pwd`:/tmp alpine-gcc:v1 /tmp/atomic/test
```

# 参考文档
[cppreference](http://zh.cppreference.com/w/%E9%A6%96%E9%A1%B5)